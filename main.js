const navigation = document.querySelector('nav');
const menuButton = document.querySelector('.hamburger');
const navigationLinks = document.querySelectorAll('nav a');


// Open/close navigation
const openMenu = () => {
  navigation.classList.toggle('active');
  menuButton.classList.toggle('active');
}

menuButton.addEventListener('click', openMenu);

const closeMenu = () => {
  navigation.classList.remove('active');
  menuButton.classList.remove('active');
}

for(const link of navigationLinks){
  link.addEventListener('click', closeMenu);
}

// Scroll to section
function clickHandler(e){
  e.preventDefault();
  const href = this.getAttribute("href");
  const offsetTop = document.querySelector(href).offsetTop;

  scroll({
    top: offsetTop,
    behavior: "smooth"
  });
}

for(const link of navigationLinks){
  link.addEventListener('click', clickHandler);
}